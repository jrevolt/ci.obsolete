#!/bin/bash
set -ua

false && source ./variables.sh

# bootstrap variables
export CIX_CITOOLS_DIR=".ci/citools"
export CIX_CITOOLS_PROJECT=${CIX_CITOOLS_PROJECT}
export CIX_CITOOLS_VERSION=${CIX_CITOOLS_VERSION}
export CIX_CITOOLS_FILE=${CIX_CITOOLS_FILE}
export CIX_CITOOLS_TYPE=$(dirname "$CIX_CITOOLS_FILE")
export CI_CONFIG_DIR="$(dirname "$CI_CONFIG_PATH")"

export CIX_GITVERSION_FILE=".git/.gitversion-${CI_PIPELINE_ID}.json"

###

callstack() { local frame=0; while caller $frame; do ((frame++)); done; }

fail() { echo "ERROR($?): $@"; callstack >&2; exit 1; return 1; }

isok() {
  local x=$?
  [ $x == 0 ] && return 0 || return $x
}

check() {
  isok && return 0 || exit 1
}

log() { echo "## $@" ; return 0; }
dbg() { ${CIX_DEBUG:-false} && log "$@"; return 0;}

quiet() { "$@" &>/dev/null; }

logrun() {
  log "$@"
  "$@"
}

#use onexit+=('command')
onexit=()
onexit() { echo; log "onexit:"; for action in "${onexit[@]:-}"; do dbg "- $action"; eval "$action"; done; }
trap onexit EXIT

#use oninit+=('command')
oninit=()
oninit() { log "oninit:"; for action in "${oninit[@]:-}"; do dbg "- $action"; eval "$action" || fail; done; }

###

# looks for a file in a cascade up to root (relative paths terminate search when parent reaches '.')
find_files() {
    local fpath="$1"
    [[ "$fpath" =~ ^"$CI_CONFIG_DIR"/ ]] || fail "Only ${CI_CONFIG_DIR}/* paths are supported here. Invalid: ${fpath}"

    # print if exists
    if [ -f "$fpath" ]; then echo "$fpath"; fi

    local parent="$(dirname "$(dirname "$fpath")")/$(basename "$fpath")"
    [[ "$parent" =~ ^"$CI_CONFIG_DIR"/ ]] || return; #end of line, not going deeper

    find_files "$parent" #recursing
}

# find most specific file
find_files_single() {
    local all="$(find_files "$1")"
    local top="$(echo "$all" | head -n1)"
    local bottom="$(echo "$all" | tail -n1)"

    if [ "$(dirname "$bottom")" = "$CI_CONFIG_DIR" ]; then
        echo "$bottom"; # user provided file, highest priority
    else
        echo "$all" | head -n1 # most most nested file, highest priority unless user provides override
    fi
}

# sorts files according to loading priority: files in parent folders go first, more nested files are loaded later and override parent
# typicaly used to load shell scripts and give nested files option to override definitions loaded from parent
# file in root is an exception to the rule: it is loaded first
find_files_loading_sort() {
    local pathname="$1"
    local fname="$(basename "$pathname")"
    local all="$(find_files "$pathname")"
    echo "$all" | tac | grep -vE "^${CI_CONFIG_DIR}/${fname}$"
    echo "$all" | grep -E "^${CI_CONFIG_DIR}/${fname}$"
}

loading=""
loaded=""

do_load() {
    local name="$1"
    local mark="@${name},"
    [[ "$loading" =~ $mark ]] && dbg "- loading in progress: $name" && return 0
    [[ "$loaded" =~ $mark ]] && dbg "- already loaded: $name" && return 0
    loading+="$mark"
    local pathname="${CIX_CITOOLS_DIR}/${CIX_CITOOLS_TYPE}/${name}"
    for i in $(find_files_loading_sort "$pathname"); do
        dbg "Loading ${i}..."
        source "$i" || fail "Error loading ${i}"
    done
    loading=${loading/$mark}
    loaded+="$mark"
}

###

ci_checkout_citools() {
  log "Initializing CI tools (${CIX_CITOOLS_PROJECT} @ ${CIX_CITOOLS_VERSION}) ..."
  mkdir -p ${CIX_CITOOLS_DIR}/.git
  (
    flock -x 200

    cd ${CIX_CITOOLS_DIR}

    local update=${CIX_DEBUG}

    local fcheckedout=".git/.ci-checkout-${CI_PIPELINE_ID}"

    if [[ -f ${fcheckedout} ]] && ! ${update}; then
      log "- already checked out for pipeline ${CI_PIPELINE_ID}"
      return 0
    fi

    local url="${CI_SERVER_URL//:\/\/*}://citools:${CIX_GITLAB_TOKEN}@${CI_SERVER_URL//*:\/\/}/${CIX_CITOOLS_PROJECT}.git"
    if [[ ! -f .git/config ]]; then
      log "- initializing git repo in $PWD"
      quiet git init .
      quiet git remote add origin "$url"
      quiet git fetch origin ${CIX_CITOOLS_VERSION}
      quiet git checkout ${CIX_CITOOLS_VERSION}
    else
      log "- updating git repo in $PWD"
      quiet git clean -dfx
      quiet git remote set-url origin "$url"
      quiet git fetch origin ${CIX_CITOOLS_VERSION}
      quiet git reset --hard origin/${CIX_CITOOLS_VERSION}
    fi
    git reset --hard ${CIX_CITOOLS_VERSION}

    touch ${fcheckedout}

    flock -u 200
  ) 200> ${CIX_CITOOLS_DIR}/.git/.ci-checkout-citools
}

ci_checkout_sources() {
  log "Checking out sources (${CI_PROJECT_PATH} @ ${CI_COMMIT_REF_NAME}) ..."
  mkdir -p ${CI_PROJECT_DIR}/.git
  (
    flock -x 200

    cd ${CI_PROJECT_DIR}

    local fcheckedout=".git/.ci-checkout-${CI_PIPELINE_ID}"

    if [[ -f ${fcheckedout} ]]; then
      log "- already checked out for pipeline ${CI_PIPELINE_ID}"
      return 0
    fi

    local url="${CI_SERVER_URL//:\/\/*}://citools:${CIX_GITLAB_TOKEN}@${CI_SERVER_URL//*:\/\/}/${CI_PROJECT_PATH}.git"
    if [[ ! -f .git/config ]]; then
      log "- initializing repository: ${PWD}"
      quiet git init .
      quiet git remote add origin ${url}
    else
      quiet git remote set-url origin ${url}
    fi
    log "- fetching updates..."
    if is_release; then
      quiet git clean -dfx
      quiet git fetch --no-tags origin +refs/tags/${CI_COMMIT_TAG}:refs/tags/${CI_COMMIT_TAG}
      git reset --hard ${CI_COMMIT_TAG}
    else
      quiet git fetch --tags origin master
      quiet git fetch --tags origin develop
      quiet git fetch --tags origin ${CI_COMMIT_REF_NAME}
      quiet git clean -dfx
      quiet git checkout ${CI_COMMIT_REF_NAME}
      git reset --hard ${CI_COMMIT_SHA}
    fi

    touch ${fcheckedout}

    gitversion /nocache > ${CIX_GITVERSION_FILE} || fail

    flock -u 200
  ) 200> ${CI_PROJECT_DIR}/.git/.ci-checkout-sources || fail
}

ci_load_variables() {
  log "Loading variables from ${CIX_CITOOLS_PROJECT}..."
  #${CIX_DEBUG} && local verbose="-v"
  ${CIX_DEBUG} && local printvar='echo "+.key+"=${"+.key+"};'
  source <(
    curl "${CI_API_V4_URL}/projects/${CIX_CITOOLS_PROJECT//\//%2F}/variables?per_page=100" \
      -H "Private-Token: ${CIX_GITLAB_TOKEN}" \
      -s ${verbose:-} \
      | jq -r '.[] | "export "+.key+"=\"${"+.key+":-\""+.value+"\"}\"; '"${printvar:-}"'"'
  )
}

ci_load() {
  log "Working directory: ${PWD}"
  do_load "core.sh"
  do_load "variables.sh"
  do_load "version.sh"
  do_load "gitlab.sh"
  do_load "docker.sh"
  do_load "sonar.sh"
  do_load "jobs.sh"
  do_load "k8s.sh"
  do_load "ci.sh"
}

ci_initialize() {
  log "Initializing..."
  do_load "preinit.sh"
  oninit
  do_load "postinit.sh"
}

log "Bootstrap loaded"
export CIX_BOOTSTRAP_LOADED=true

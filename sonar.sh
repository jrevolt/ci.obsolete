#!/bin/bash
set -ua

false && source ./ci.sh

sonar_initialize() {
  export CIX_SONAR_PROJECT=$(
    {
    is_release \
    && echo ${CI_PROJECT_PATH//\//:} \
    || echo ${CI_PROJECT_PATH//\//:}:${CI_COMMIT_REF_NAME}
    } | sed -re 's/[^:_.[:alpha:]]/-/g'
  )

  buildctl_secrets+=(id=ciadmin,src=.git/ciadmin.yaml)
  sonar_generate_ciadmin > .git/ciadmin.yaml
}

oninit+=(sonar_initialize)

sonar_generate_ciadmin() {
cat <<EOF
---
ciadmin:
  sonar:
    url: ${CIX_SONAR_URL}
    token: ${CIX_SONAR_TOKEN}
  ci:
    sonar:
      defaultQualityProfileName: ${CIX_SONAR_DEFAULT_QUALITY_PROFILE:-"jrevolt-default"}
  groups:
  - name: ${CIX_GITLAB_GROUP}
    projects:
    - name: ${CIX_GITLAB_PROJECT}
      repositories:
      - name: ${CIX_GITLAB_REPO}
        options: { languages: [ ${CIX_SONAR_LANGUAGES:-} ] }

EOF
}

sonar_curl() {
    curl -su "${CIX_SONAR_TOKEN}:" "$@" --fail || fail
}

sonar_curl_api() {
    local url="${CIX_SONAR_URL}/api/$1"; shift
    sonar_curl -XPOST "${url}" "$@" || fail
}

sonar_check_analysis_status() {
    local started=$(date +%s)

    ${CIX_DEBUG} && cat /tmp/${CI_JOB_ID}/report-task.txt

    source /tmp/${CI_JOB_ID}/report-task.txt || fail

    # expected to be loaded; also validates; also makes IDE happy
    local dashboardUrl="${dashboardUrl}"
    local projectKey="${projectKey}"
    local ceTaskId="${ceTaskId}"
    local ceTaskUrl="${ceTaskUrl}"

    log "sonar: waiting for task: ${ceTaskId:-} ..."
    while :; do
        [[ "$(sonar_curl ${ceTaskUrl} | jq -r '.task.status')" =~ "PENDING|IN_PROGRESS" ]] || break;
        sleep 1s
    done

    log "sonar: waiting for analysisId ..."
    local analysisId=
    while :; do
        local analysisId="$(sonar_curl ${ceTaskUrl} | jq -r '.task.analysisId | select (.!=null)')"
        [[ "$analysisId" = "" ]] || break
        sleep 1s
    done

    log "sonar: waiting for quality gate (analysisId=$analysisId) ..."
    local sonarStatus
    while :; do
        sonarStatus=$(sonar_curl_api qualitygates/project_status -d analysisId="$analysisId" | jq -r '.projectStatus.status')
        [[ "$sonarStatus" = "" ]] || break
        sleep 1s
    done

    log "sonar: project status: ${sonarStatus:-}"
    local failed=false
    [[ "$sonarStatus" = "ERROR" ]] && {
        failed=true
        sonar_curl_api issues/search \
            -d componentKeys=${projectKey} -d resolved=false -d sinceLeakPeriod=true \
            | jq .issues
        log "Sonar reports errors: ${CIX_SONAR_URL}/dashboard?id=${projectKey}"
    }

    local finished=$(date +%s)
    log "sonar: total wait time: $(($finished-$started))s"

    if ${failed}; then fail; fi
}

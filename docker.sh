#!/bin/bash
set -ua

override_dockerignore() {
cat <<EOF
.dockerignore
.git
.ci/citools/.git
.ci/citools/.ci
.ci/Dockerfile
EOF
}

docker_override_dockerignore() {
  override_dockerignore > .dockerignore
}

oninit+=(docker_override_dockerignore)

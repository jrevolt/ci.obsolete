#!/bin/bash
set -ua

# IDE support: imports
false && source ./ci.sh
false && source ./variables.sh

# IDE support: variables
false && CIX_K8S_DBSERVER=
false && CIX_K8S_DBNAME=
false && CIX_K8S_DBUSER=
false && CIX_K8S_DBPASS=

k8s_initialize() {

    # gitlab project == rancher project == k8s namespace
    export CIX_K8S_PROJECT="${CIX_GITLAB_GROUP//\//-}-${CIX_GITLAB_PROJECT}"
    export CIX_K8S_NAMESPACE="${CIX_GITLAB_GROUP//\//-}-${CIX_GITLAB_PROJECT}"

    export CIX_K8S_ROOT_DOMAIN="${CIX_K8S_ROOT_DOMAIN}"
    export CIX_K8S_DOMAIN="${CIX_K8S_NAMESPACE}.${CIX_K8S_ROOT_DOMAIN}"

    CIX_K8S_HOSTNAME=;
    is_release &&
      CIX_K8S_HOSTNAME=${CIX_K8S_DOMAIN} ||
      CIX_K8S_HOSTNAME="${CI_COMMIT_REF_SLUG}.${CIX_K8S_DOMAIN}"
    export CIX_K8S_HOSTNAME

    export CIX_K8S_APPLICATION=${CIX_K8S_PROJECT}
    export CIX_K8S_COMPONENT=${CIX_GITLAB_REPO}
    export CIX_K8S_VERSION="$CIX_APPVERSION"
    export CIX_K8S_CONTAINER_PORT="${CIX_K8S_CONTAINER_PORT:-80}"
    export CIX_K8S_DEPLOYMENT=$(k8s_determine_deployment_name)
    export CIX_K8S_WORKLOAD="${CIX_K8S_DEPLOYMENT}-${CIX_K8S_COMPONENT}"

}

oninit+=(k8s_initialize)

k8s_determine_deployment_name() {
    # goal: sort deployments by priority:
    # - release,
    # - master branch,
    # - develop branch (if any)
    # - and rest of the branches
    # also: branches created in GitLab from issues start with number which is invalid for k8s
    is_release  && echo "s0-release" && return
    is_master   && echo "s1-master" && return
    is_develop  && echo "s2-develop" && return
    echo                "s3-${CI_COMMIT_REF_SLUG}"
}

k8s_helm_values_custom() { return 0; }

k8s_helm_values() {
local ver=$(echo "$CIX_GITVERSION" | jq -r .FullBuildMetaData) || fail
local custom=$(k8s_helm_values_custom) || fail
cat <<EOF
deployment: ${CIX_K8S_DEPLOYMENT}
component: ${CIX_K8S_COMPONENT}
name: ${CIX_K8S_WORKLOAD}
image: ${CIX_REPO_DOCKER}/${CI_PROJECT_PATH}:${CIX_APPVERSION_DOCKER}
citools:
  pipeline: ${CI_PIPELINE_ID}
  job: ${CI_JOB_ID}
  gitversion: ${ver}
${custom}
EOF
}

k8s_helm_upgrade() {
  ${CIX_DEBUG} && local debug="--debug"

  local values
  values=$(k8s_helm_values) || fail

  helm upgrade --install -n ${CIX_K8S_NAMESPACE} \
    ${CIX_K8S_WORKLOAD} ${CIX_CITOOLS_DIR}/${CIX_CITOOLS_TYPE} \
    --history-max 3 \
    ${debug:-} \
    -f - < <(echo "$values")
}

k8s_get_dbname() {
    local dbname=
    if is_release; then
        dbname="${CIX_GITLAB_PROJECT}";
    else
        dbname="${CIX_GITLAB_PROJECT}-${CI_COMMIT_REF_SLUG}"
    fi
    echo "${dbname//-/_}"
    return 0
}


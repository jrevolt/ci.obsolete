#!/bin/bash
set -u

isok() {
  [ $? == 0 ] && return 0 || return 1
}

check() {
  isok || exit 1
}

f1() {
local x
IFS='' read -d '' x <<EOF
4
5
6
EOF
echo "exit:$?"
echo "$x"
}

f() {
local x
! IFS='' read -d '' x <<EOF
1
2
3
$(exit 2)
EOF
echo "exit:$?"
echo "$x"
}

check() {
  [ $? == 0 ] || return 1
  cat -
}

f2() {
cat <<EOF
1
2
$(exit 1)
$(exit 0)
EOF
}

f2
echo "exit:$?"

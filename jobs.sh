#!/bin/bash
set -ua

false && source ./ci.sh

wait_job() {
  local name="$1"
  log "Waiting for job $name..."
  local url="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs?per_page=100"
  local auth="Private-Token: ${CIX_GITLAB_TOKEN}"
  while true; do
    # find by name, select latest
    local status=$(curl -sf -H "$auth" "$url" | jq -r "map(select(.name==\"$name\"))[-1].status") || fail
    case "$status" in
      success)
        log "...completed (status=$status)"
        return 0
        ;;
      created|pending|running|manual)
        log "...waiting (status=$status)"
        sleep 5s
        ;;
      failed|canceled|skipped)
        fail "Failed waiting for job '$name', status=$status"
        ;;
      *)
        fail "status=$status"
        ;;
    esac
  done
}

wait_jobs() {
  for i in ${needs:-}; do wait_job $i; done
}

job() {
  set -u
  local action="$CI_JOB_NAME"
  action=${action// /_}
  action=${action//./_}
  log "job: ${CI_JOB_NAME} => ${action}"
  "${action}"
}

buildctl_args=()
buildctl_secrets=()

buildctl_args() {
  for i in "${buildctl_args[@]:-}"; do
    echo "--opt build-arg:${i}"
  done
}

buildctl() {
  local target="$1"; shift
  local cache_version
  cache_version=$(
    is_release && echo "latest" || echo "$CI_COMMIT_REF_SLUG"
  )
  local nocache_opt
  if ${CIX_BUILDKIT_NO_CACHE:-false}; then
    nocache_opt="--no-cache"
  fi
  local jobs
  local import_cache_opt
  if ${import_cache:-true}; then
    jobs="$(list_pipeline_jobs)"
    import_cache_opt="$(
    for i in ${jobs}; do
      #fixme: don't import caches that may never exist (never exported)
      i=${i//./}; i=${i,,}
      echo "--import-cache type=registry,ref=${CIX_REGISTRY}/${CI_PROJECT_PATH}/cache/${i}:${cache_version}"
    done)"
  fi
  local export_cache_opt
  if ${export_cache:-true}; then
    export_cache_opt="$(
    id=${CI_JOB_NAME}; id=${id//./}; id=${id,,}
    echo "--export-cache type=registry,ref=${CIX_REGISTRY}/${CI_PROJECT_PATH}/cache/${id}:${cache_version},mode=max,push=true"
    )"
  fi
  local output_opt
  if ${output_image:-false}; then
    output_opt="--output type=image,name=${CIX_REPO_DOCKER_PUBLISH}/${CI_PROJECT_PATH}:${CIX_APPVERSION_DOCKER},push=true"
  fi
  if ${output_local:-false}; then
    output_opt="--output type=local,dest=/tmp/${CI_JOB_ID}"
  fi

  local buildargs=()
  for i in "${buildctl_args[@]}"; do buildargs+=(--opt "build-arg:$i"); done

  local secrets=()
  for i in "${buildctl_secrets[@]}"; do secrets+=(--secret "$i"); done

  entrypoint.sh client

  local dockerfile="$(dirname ${CI_CONFIG_PATH})/Dockerfile"
  (
  flock -x 200
  df_default="${CIX_CITOOLS_DIR}/${CIX_CITOOLS_TYPE}/Dockerfile"
  df_override="$(dirname ${CI_CONFIG_PATH})/ci.dockerfile"
  > ${dockerfile}
  for i in ${df_default} ${df_override}; do
    [ -f ${i} ] && cat ${i} >> ${dockerfile}
  done
  flock -u 200
  ) 200> ${CI_PROJECT_DIR}/.git/.ci-prepare-dockerfile

  log "Dockerfile:\n$(cat $dockerfile)"

  logrun $(which buildctl) \
    build \
    --frontend dockerfile.v0 \
    --local context=. \
    --local dockerfile=$(dirname $(realpath --relative-to ${CI_PROJECT_DIR} $dockerfile)) \
    --opt target=${target} \
    ${nocache_opt:-} \
    ${import_cache_opt:-} \
    ${export_cache_opt:-} \
    "${buildargs[@]}" \
    "${secrets[@]}" \
    ${output_opt:-} \
    "$@"
    local code=$?
    log "exit code: $code"
    return ${code}
}

buildctl_noexport() {
  export_cache=false buildctl "$@"
}

buildctl_image() {
  output_image=true buildctl "$@"
}

buildctl_local() {
  output_local=true buildctl "$@"
}

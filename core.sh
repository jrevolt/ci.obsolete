#!/bin/bash
set -ua

false && source ./bootstrap.sh

copy_function() {
    local name="$1"
    local newname="$2"
    local def="$(declare -f ${name})"
    local openbracket="$(echo "$def" | head -n1 | sed -re 's/[^{]+//')" || fail
    eval "$(echo "${newname} () ${openbracket}"; echo "$def" | tail -n +2)" || fail
}

cancel_outdated_pipelines() {
  log "Checking for outdated running pipelines..."
  local pipeline="$CI_PIPELINE_ID"
  local url="$CI_API_V4_URL"
  local token="$CIX_GITLAB_TOKEN"
  local project="$CI_PROJECT_ID"
  local ref="$CI_COMMIT_REF_NAME"
  local ids=$(
    curl -sk -H "Private-Token: ${token}" \
      "${url}/projects/${project}/pipelines?scope=running&ref=${ref}" \
      | jq -r '.[].id' | sort)
  local cancel=false
  for i in ${ids}; do
    (( ${i} >= ${pipeline} )) && continue #don't kill yourself or any younger siblings :-)
    log "Cancelling running pipeline ${i}..."
    curl -sk -XPOST -H "Private-Token: ${token}" ${url}/projects/${project}/pipelines/${i}/cancel >/dev/null
    cancel=true
  done
  if ${cancel}; then
    log "Give canceled pipelines few seconds to shut down..."
    sleep 10s
  else
    log "- no concurrent running pipelines found for ref=${ref}."
  fi
}

list_pipeline_jobs() {
  curl -s -H "Private-Token: ${CIX_GITLAB_TOKEN}" \
    ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs \
    | jq -r '.[].name'
}

is_release() {
    [ "${CI_COMMIT_TAG:-}" != "" ] || return 1
}

is_master() {
    [ "$CI_COMMIT_REF_SLUG" = "master" ] || return 1
}

is_develop() {
    [ "$CI_COMMIT_REF_SLUG" = "develop" ] || return 1
}

is_feature() {
    is_release && return 1
    is_master && return 1
    return 0
}

is_release_or_master() {
    is_release || is_master || return 1
}

is_branch() {
    is_release && return 1 || return 0
}




#!/bin/bash
set -ua

false && source ./../ci.sh

init_dotnet_webapi() {
  buildctl_args+=("CIX_DOCKER_IMAGE_RUNTIME=${CIX_DOCKER_IMAGE_RUNTIME}")
}

oninit+=(init_dotnet_webapi)

B2_image() {
  buildctl runtime
}

P1_image() {
  output_image=true \
  buildctl runtime
}

P2_deploy() {
  k8s_helm_upgrade
}

k8s_helm_values_custom() {
  local dbname=$(k8s_get_dbname) || fail
  cat <<EOF
database:
  enable: true
  server: ${CIX_K8S_DBSERVER}
  name: ${dbname}
  user: ${CIX_K8S_DBUSER}
  pass: ${CIX_K8S_DBPASS}
ingress:
  hostname: ${CIX_K8S_HOSTNAME}
  domain: ${CIX_K8S_DOMAIN}
  issuer: ${CIX_K8S_TLS_ISSUER}
  context: ${CIX_K8S_DEPLOYMENT_CONTEXT:-/}
  paths: ${CIX_K8S_DEPLOYMENT_PATHS:-[]}
EOF
}

## flatten yaml:
# cat dotnet/webapi/data.yaml| yq -r 'leaf_paths as $path | {key: $path|join("."), value: getpath($path)} | ""+.key+"="+.value'

## merge yamls:
# yq -s 'reduce .[] as $x ({}; . * $x)'

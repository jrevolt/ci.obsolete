#!/bin/bash
set -ua

false && source ../ci.sh

init_dotnet_classlib() {
  #buildctl_args+=("")
  return 0
}

oninit+=(init_dotnet_classlib)

P1_nuget() {
  export_cache=false \
  buildctl publish
}

#!/bin/bash
set -ua

false && source ../ci.sh

init_dotnet() {
  export CIX_REPO_NUGET_PUBLISH=$(
    is_release && echo ${CIX_REPO_NUGET_RELEASES} || echo ${CIX_REPO_NUGET_SNAPSHOTS}
  )
  buildctl_args+=("CIX_REPO_NUGET=${CIX_REPO_NUGET}")
  buildctl_args+=("CIX_REPO_NUGET_PUBLISH=${CIX_REPO_NUGET_PUBLISH}")
  buildctl_args+=("CIX_REPO_NUGET_APIKEY=${CIX_REPO_NUGET_APIKEY}")
  buildctl_args+=("CIX_DOTNET_MAIN=${CIX_DOTNET_MAIN:-Main}")
  buildctl_args+=("CIX_DOTNET_TEST_LOGGER=${CIX_DOTNET_TEST_LOGGER}")
  buildctl_args+=("CIX_SONAR_PARAMETERS=
  /key:${CIX_SONAR_PROJECT}
  /version:${CIX_APPVERSION}
  /d:sonar.host.url=${CIX_SONAR_URL}
  /d:sonar.login=${CIX_SONAR_TOKEN}
  /d:sonar.leak.period=BASELINE
  /d:sonar.cs.xunit.reportsPaths=**/TestResults.xml
  /d:sonar.cs.nunit.reportsPaths=**/TestResults.xml
  /d:sonar.cs.opencover.reportsPaths=**/TestCoverage.xml
  ")
  buildctl_args+=("CIX_SONAR_TOKEN=${CIX_SONAR_TOKEN}")
}

oninit+=(init_dotnet)

prepare() {
  buildctl base;
}

B1_build() {
  buildctl build
}

T1_test() {
  export_cache=false output_local=true \
  buildctl test
  cat /tmp/${CI_JOB_ID}/test.out
  return $(cat /tmp/${CI_JOB_ID}/test.status)
}

T2_sonar() {
  export_cache=false output_local=true \
  buildctl sonar
  cat /tmp/${CI_JOB_ID}/sonar.out
  local result=$(cat /tmp/${CI_JOB_ID}/sonar.status)
  if [[ ${result} != 0 ]]; then fail; fi
  sonar_check_analysis_status
}

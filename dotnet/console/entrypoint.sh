#!/bin/bash
set -u

BASEDIR=/app
MAIN_DLL=${BASEDIR}/$(basename ${BASEDIR}/*.runtimeconfig.json .runtimeconfig.json).dll

finish() {
	echo "$(date) Shutting down..."
	local pid=$(pgrep -f $MAIN_DLL)
	kill -0 $pid 2>/dev/null && kill -INT $pid && wait $pid
	echo "$(date) Shutdown complete!"
}

trap finish EXIT

dotnet $MAIN_DLL "$@"

#!/bin/bash
set -ua

false && source ../ci.sh

init_dotnet_console() {
  buildctl_args+=("CIX_DOCKER_IMAGE_RUNTIME=${CIX_DOCKER_IMAGE_RUNTIME}")
}

oninit+=(init_dotnet_console)

B2_image() {
  buildctl runtime
}

P1_image() {
  output_image=true \
  buildctl runtime
}

P2_deploy() {
  k8s_helm_upgrade
}



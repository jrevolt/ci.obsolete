#!/bin/bash
set -ua

false && source ./ci.sh

false && export CIX_APPVERSION_DOCKER=

appversion() {
  export CIX_GITVERSION="$(<${CIX_GITVERSION_FILE})"
  echo "${CIX_GITVERSION}"
}

version_initialize() {
  appversion

  export CIX_APPVERSION_SELECTOR=$(
    is_release &&
    echo "$CIX_APPVERSION_SELECTOR_TAG" ||
    echo "$CIX_APPVERSION_SELECTOR_BRANCH"
  )
  export CIX_APPVERSION_DOCKER_SELECTOR=$(
    is_release &&
    echo "$CIX_APPVERSION_DOCKER_SELECTOR_TAG" ||
    echo "$CIX_APPVERSION_DOCKER_SELECTOR_BRANCH"
  )

  export CIX_APPVERSION="$(echo "$CIX_GITVERSION" | jq -r "${CIX_APPVERSION_SELECTOR}")"
  log "CIX_APPVERSION=${CIX_APPVERSION}"

  export CIX_APPVERSION_DOCKER="$(echo "$CIX_GITVERSION" | jq -r "${CIX_APPVERSION_DOCKER_SELECTOR}")"
  log "CIX_APPVERSION_DOCKER=${CIX_APPVERSION_DOCKER}"
}

oninit+=(version_initialize)

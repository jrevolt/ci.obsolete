#!/bin/bash
set -ua

# IDE support
false && source ./bootstrap.sh
false && source ./core.sh
false && source ./utils.sh
false && source ./variables.sh
false && source ./version.sh
false && source ./docker.sh
false && source ./k8s.sh
false && source ./jobs.sh
false && source ./sonar.sh

init_variables() {
  export CIX_REPO_DOCKER_PUBLISH=$(
    is_release && echo ${CIX_REPO_DOCKER_RELEASES} || echo ${CIX_REPO_DOCKER_SNAPSHOTS}
  )
  export CIX_PROJECT_REF=$(
    is_release \
    && echo "${CI_PROJECT_PATH}" \
    || echo "${CI_PROJECT_PATH}:${CI_COMMIT_REF_NAME}"
  )
}

init_build_args() {
  buildctl_args+=("http_proxy=${http_proxy:-}")
  buildctl_args+=("https_proxy=${https_proxy:-}")
  buildctl_args+=("no_proxy=${no_proxy:-}")
  buildctl_args+=("CI_PIPELINE_ID=${CI_PIPELINE_ID}")
  buildctl_args+=("CI_JOB_ID=${CI_JOB_ID}")
  buildctl_args+=("CI_PROJECT_PATH=${CI_PROJECT_PATH}")
  buildctl_args+=("CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME}")
  buildctl_args+=("CI_COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG}")
  buildctl_args+=("CIX_APPVERSION=${CIX_APPVERSION}")
  buildctl_args+=("CIX_DOCKER_IMAGE_SDK=${CIX_DOCKER_IMAGE_SDK}")
  buildctl_args+=("CIX_PROJECT_REF=${CIX_PROJECT_REF}")
  buildctl_args+=("CIX_DOCKER_IMAGE_SDK_EXTRA_PACKAGES=${CIX_DOCKER_IMAGE_SDK_EXTRA_PACKAGES}")
}

init_build_secrets() {
  buildctl_secrets+=("id=cacerts,src=/etc/ssl/certs/ca-certificates.crt")
}

oninit+=(init_variables)
oninit+=(init_build_args)
oninit+=(init_build_secrets)


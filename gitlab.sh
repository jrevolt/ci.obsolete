#!/bin/bash
set -ua

false && source ./ci.sh

gitlab_initialize() {
    # CI_PROJECT_PATH=group/subgroup/project/repo => group/subgroup project repo
    IFS=' ' read -r -a parsed <<< "$(echo ${CI_PROJECT_PATH} | sed -re 's/\/([^/]+)\/([^/]+)$/ \1 \2/')"
    export CIX_GITLAB_GROUP="${parsed[0]}"
    export CIX_GITLAB_PROJECT="${parsed[1]}"
    export CIX_GITLAB_REPO="${parsed[2]}"
}

oninit+=(gitlab_initialize)

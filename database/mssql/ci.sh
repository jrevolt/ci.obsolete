#!/bin/bash
set -ua

false && source ./../../k8s.sh

init_database_mssql() {
  export CIX_TEST_DBNAME="test_${CI_JOB_ID}"
  buildctl_args+=("CIX_DOCKER_IMAGE_RUNTIME=${CIX_DOCKER_IMAGE_RUNTIME}")
  buildctl_args+=("CIX_TEST_DBSERVER=${CIX_TEST_DBSERVER}")
  buildctl_args+=("CIX_TEST_DBUSER=${CIX_TEST_DBUSER}")
  buildctl_args+=("CIX_TEST_DBPASS=${CIX_TEST_DBPASS}")
  buildctl_args+=("CIX_TEST_DBNAME=${CIX_TEST_DBNAME}")
}

oninit+=(init_database_mssql)

prepare() {
  #find * -type f | grep -E '\.(sln|sqlproj|props|targets)$' | xargs tar cvf .restore.tar
  #tar cf .sources.tar *
  buildctl base;
}

B1_build() { buildctl build; }
B2_image() { buildctl runtime; }
T1_test() { buildctl test; }

P1_image() {
  output_image=true \
  buildctl runtime
}

P2_deploy() {
  log "Uninstalling old deployment job, if any..."
  helm -n ${CIX_K8S_NAMESPACE} uninstall ${CIX_K8S_WORKLOAD} || true
  log "Installing deployment job..."
  k8s_helm_upgrade &&
  log "- waiting for job..."
  kubectl -n ${CIX_K8S_NAMESPACE} wait pod -l job-name=${CIX_K8S_WORKLOAD} --for condition=Initialized --timeout=60s  &&
  (
  while ! kubectl -n ${CIX_K8S_NAMESPACE} logs -l job-name=${CIX_K8S_WORKLOAD} --tail=0; do
    log "- still waiting..."
    sleep 5s;
  done
  ) &&
  kubectl -n ${CIX_K8S_NAMESPACE} logs -l job-name=${CIX_K8S_WORKLOAD} -f --tail=-1
}

k8s_helm_values_custom() {
local dbname=$(k8s_get_dbname) || fail
cat <<EOF
appsettings:
  server:   "${CIX_K8S_DBSERVER}"
  database: "${CIX_K8S_DBNAME:-$dbname}"
  username: "${CIX_K8S_DBUSER}"
  password: "${CIX_K8S_DBPASS}"
EOF
}
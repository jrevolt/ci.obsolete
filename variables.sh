#!/bin/bash
set -ua

# gitlab
false && CI_BUILDS_DIR=
false && CI_COMMIT_REF_NAME=
false && CI_COMMIT_REF_SLUG=
false && CI_COMMIT_SHA=
false && CI_COMMIT_TAG=
false && CI_JOB_TOKEN=
false && CI_PIPELINE_ID=
false && CI_PROJECT_ID=
false && CI_PROJECT_PATH=
false && CI_PROJECT_PATH_SLUG=
false && CI_REPOSITORY_URL=
false && CI_SERVER_URL=
false && CI_JOB_NAME=
false && CI_JOB_ID=
false && CI_CONFIG_PATH=
false && CI_API_V4_URL=

# citools
false && CIX_DEBUG=
false && CIX_CITOOLS_FILE=
false && CIX_CITOOLS_PROJECT=
false && CIX_CITOOLS_VERSION=
false && CIX_GITLAB_TOKEN=
false && CIX_REPO_DOCKER=
false && CIX_REPO_DOCKER_RELEASES=
false && CIX_REPO_DOCKER_SNAPSHOTS=

# sonar
false && CIX_SONAR_URL=
false && CIX_SONAR_TOKEN=


true

#!/bin/bash
set -ua

false && source ./../ci.sh

init_nodejs_reactapp() {
  buildctl_args+=("CIX_DOCKER_IMAGE_RUNTIME=${CIX_DOCKER_IMAGE_RUNTIME}")
}

oninit+=(init_nodejs_reactapp)

prepare() {
  buildctl base;
}

B2_image() { buildctl runtime; }

P1_image() { buildctl_image runtime; }

P2_deploy() { k8s_helm_upgrade; }

k8s_helm_values_custom() {
cat << EOF
ingress:
  hostname: ${CIX_K8S_HOSTNAME}
  domain: ${CIX_K8S_DOMAIN}
  issuer: ${CIX_K8S_TLS_ISSUER}
  context: ${CIX_K8S_DEPLOYMENT_CONTEXT:-/}
  paths: ${CIX_K8S_DEPLOYMENT_PATHS:-[]}
EOF
}

#!/bin/sh
set -u

prefix=$(echo "${PREFIX:-}" | sed -re '
s/^/\//;    # leading slash
s/\/+/\//g; # normalize/deduplicate
s/\/$//;    # drop trailing
s/^$/\//;   # empty => /
')
port="${PORT:-80}"

# /etc/hosts entry avoids DNS lookup delays and errors when resolving self hostname
printf "127.0.0.1 $(hostname)" >> /etc/hosts

echo "Configuring content to serve on path: $prefix"

# create deployment directory
sitedir="/tmp/site"
appdir="${sitedir}/${prefix}"

# copy content to site directory
mkdir -p "$appdir"
rsync /app/ "$appdir/" -ar --delete

cd "$appdir"

replacement="$prefix"
[ "$replacement" == "/" ] && replacement=""

echo "Deploying static content on '${prefix}'. Replacing prefix placeholders in all static files (ignoring media)"
sed -i -re 's/\/*PREFIX_PLACEHOLDER/'${replacement//\//\\\/}'/g' $(find -type f | grep -vE '\.(jpg|jpeg|png|svg)$')

if [ -f "env.${NODE_ENV}.js" ]; then
    echo "Deploying env.${NODE_ENV}.js => env.js"
    cat "env.${NODE_ENV}.js" > env.js
else
    echo "Missing env.${NODE_ENV}.js. Using default env.js (if any)"
fi

# QDH for deployments on specific $prefix
if [[ "$(realpath $sitedir)" != "$(realpath $appdir)" ]]; then
    tar c -C ${appdir} . | tar x -C ${sitedir}
fi

# launch the server
serve -l $port --single "$@" "$sitedir"

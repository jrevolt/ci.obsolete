#!/bin/bash
set -ua

false && source ./../ci.sh

init_nodejs_npm() {
  export CIX_REPO_NPM_PUBLISH=$(
    is_release && echo ${CIX_REPO_NPM_RELEASES} || echo ${CIX_REPO_NPM_SNAPSHOTS}
  )
  buildctl_args+=("CIX_REPO_NPM_PUBLISH=${CIX_REPO_NPM_PUBLISH}")
  buildctl_args+=("CIX_REPO_NPM_CREDENTIALS=$(echo -n "$CIX_REPO_USER:$CIX_REPO_PASS" | base64)")
}

oninit+=(init_nodejs_npm)


P1_npm() { buildctl publish; }

#!/bin/bash
set -ua

false && source ./../ci.sh

init_nodejs() {
  buildctl_args+=("CIX_REPO_NPM=${CIX_REPO_NPM}")
}

oninit+=(init_nodejs)

prepare() {
  buildctl base;
}

B1_build() { buildctl build; }

T1_test() {
  export_cache=false output_local=true \
  buildctl test
  cat /tmp/${CI_JOB_ID}/test.out
  return $(cat /tmp/${CI_JOB_ID}/test.status)
}
T2_audit() {
  export_cache=false output_local=true \
  buildctl audit
  cat /tmp/${CI_JOB_ID}/audit.out
  return $(cat /tmp/${CI_JOB_ID}/audit.status)
}
T3_eslint() {
  export_cache=false output_local=true \
  buildctl eslint
  cat /tmp/${CI_JOB_ID}/eslint.out
  cat /tmp/${CI_JOB_ID}/eslint.status
  return $(cat /tmp/${CI_JOB_ID}/eslint.status)
}
